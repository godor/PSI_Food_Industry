/**
 * 快递公司 - 主界面
 */
Ext.define("PSI.Express.CompanyMainForm", {
	extend : "Ext.panel.Panel",
	border : 0,
	layout : "border",

	/**
	 * 初始化组件
	 */
	initComponent : function() {
		var me = this;

		Ext.apply(me, {
					tbar : [{
								text : "新增快递公司",
								iconCls : "PSI-button-add",
								handler : me.onAddCompany,
								scope : me
							}, {
								text : "编辑快递公司",
								iconCls : "PSI-button-edit",
								handler : me.onEditCompany,
								scope : me
							}, {
								text : "删除快递公司",
								iconCls : "PSI-button-delete",
								handler : me.onDeleteCompany,
								scope : me
							}, "-", {
								text : "编辑快递单打印项",
								iconCls : "PSI-button-edit",
								handler : me.onEditBill,
								scope : me
							}, "-", {
								text : "设计打印样式",
								iconCls : "PSI-button-edit",
								handler : me.onDesignBill,
								scope : me
							}, "-", {
								text : "重置打印样式",
								iconCls : "PSI-button-delete",
								handler : me.onDeleteDesignBill,
								scope : me
							}, "-", {
								text : "设置发件人信息",
								iconCls : "PSI-button-edit",
								handler : me.onSenderInfo,
								scope : me
							}, "-", {
								text : "关闭",
								iconCls : "PSI-button-exit",
								handler : function() {
									location.replace(PSI.Const.BASE_URL);
								}
							}],
					items : [{
								region : "center",
								xtype : "panel",
								layout : "border",
								border : 0,
								items : [{
											region : "west",
											layout : "fit",
											width : 500,
											items : [me.getMainGrid()]
										}, {
											region : "center",
											layout : "border",
											items : [{
														region : "north",
														xtype : "panel",
														title : "快递单打印模版",
														height : 140,
														layout : {
															type : "table",
															columns : 2
														},
														border : 0,
														items : me.getPageCmp()
													}, {
														region : "center",
														layout : "fit",
														border : 0,
														items : [me
																.getPageItemGrid()]
													}]
										}]
							}]
				});

		me.callParent(arguments);

		me.freshGrid();
	},

	/**
	 * 新建
	 */
	onAddCompany : function() {
		var form = Ext.create("PSI.Express.CompanyEditForm", {
					parentForm : this
				});

		form.show();
	},

	/**
	 * 编辑
	 */
	onEditCompany : function() {
		var me = this;

		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择要编辑的快递公司");
			return;
		}

		var c = item[0];

		var form = Ext.create("PSI.Express.CompanyEditForm", {
					parentForm : me,
					entity : c
				});

		form.show();
	},

	/**
	 * 删除
	 */
	onDeleteCompany : function() {
		var me = this;
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择要删除的快递公司");
			return;
		}

		var mode = item[0];
		var info = "请确认是否删除快递公司 <span style='color:red'>" + mode.get("name")
				+ "</span> ?";

		var store = me.getMainGrid().getStore();
		var index = store.findExact("id", mode.get("id"));
		index--;
		var preIndex = null;
		var pre = store.getAt(index);
		if (pre) {
			preIndex = pre.get("id");
		}

		var funcConfirm = function() {
			var el = Ext.getBody();
			el.mask(PSI.Const.LOADING);
			var r = {
				url : PSI.Const.BASE_URL + "Home/Express/deleteCompany",
				params : {
					id : mode.get("id")
				},
				method : "POST",
				callback : function(options, success, response) {
					el.unmask();
					if (success) {
						var data = Ext.JSON.decode(response.responseText);
						if (data.success) {
							PSI.MsgBox.tip("成功完成删除操作");
							me.freshGrid(preIndex);
						} else {
							PSI.MsgBox.showInfo(data.msg);
						}
					} else {
						PSI.MsgBox.showInfo("网络错误");
					}
				}
			};

			Ext.Ajax.request(r);
		};

		PSI.MsgBox.confirm(info, funcConfirm);
	},

	isIE : function() {
		if (!!window.ActiveXObject || "ActiveXObject" in window)
			return true;
		else
			return false;
	},

	onDesignBill : function() {
		var me = this;
		if (!me.isIE()) {
			PSI.MsgBox.showInfo("设计打印样式只支持在IE浏览器下使用");
			return;
		}

		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择快递公司");
			return;
		}

		var express = item[0];
		var form = Ext.create("PSI.Express.BillTemplateDesignForm", {
					parentForm : me,
					entity : express
				});

		form.show();
	},

	onDeleteDesignBill : function() {
		var me = this;
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择快递公司");
			return;
		}

		var express = item[0];
		var info = "请确认是否重置打印样式?";

		var funcConfirm = function() {
			var el = Ext.getBody();
			el.mask(PSI.Const.LOADING);
			var r = {
				url : PSI.Const.BASE_URL + "Home/Express/deleteDesignBill",
				params : {
					id : express.get("id")
				},
				method : "POST",
				callback : function(options, success, response) {
					el.unmask();
					if (success) {
						var data = Ext.JSON.decode(response.responseText);
						if (data.success) {
							PSI.MsgBox.tip("成功完成删除操作");
							me.refreshTemplateInfo();
						} else {
							PSI.MsgBox.showInfo(data.msg);
						}
					} else {
						PSI.MsgBox.showInfo("网络错误");
					}
				}
			};

			Ext.Ajax.request(r);
		};

		PSI.MsgBox.confirm(info, funcConfirm);
	},

	onEditBill : function() {
		var me = this;
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			PSI.MsgBox.showInfo("请选择快递公司");
			return;
		}

		var express = item[0];

		var form = Ext.create("PSI.Express.BillTemplateEditForm", {
					parentForm : me,
					entity : express
				});

		form.show();
	},

	onSenderInfo : function() {
		var form = Ext.create("PSI.Express.SenderEditForm");
		form.show();
	},

	freshGrid : function(id) {
		var me = this;
		var grid = me.getMainGrid();
		var el = grid.getEl() || Ext.getBody();
		el.mask(PSI.Const.LOADING);
		Ext.Ajax.request({
					url : PSI.Const.BASE_URL + "Home/Express/companyList",
					method : "POST",
					callback : function(options, success, response) {
						var store = grid.getStore();

						store.removeAll();

						if (success) {
							var data = Ext.JSON.decode(response.responseText);
							store.add(data);

							me.gotoGridRecord(id);
						}

						el.unmask();
					}
				});
	},

	gotoGridRecord : function(id) {
		var me = this;
		var grid = me.getMainGrid();
		var store = grid.getStore();
		if (id) {
			var r = store.findExact("id", id);
			if (r != -1) {
				grid.getSelectionModel().select(r);
			} else {
				grid.getSelectionModel().select(0);
			}
		}
	},

	getMainGrid : function() {
		var me = this;
		if (me.__mainGrid) {
			return me.__mainGrid;
		}

		var modelName = "PSIExpressCompany";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "name"]
				});

		me.__mainGrid = Ext.create("Ext.grid.Panel", {
					border : 0,
					viewConfig : {
						enableTextSelection : true
					},
					columnLines : true,
					columns : [{
								xtype : "rownumberer"
							}, {
								header : "快递公司",
								dataIndex : "name",
								menuDisabled : true,
								sortable : false,
								width : 400
							}],
					store : Ext.create("Ext.data.Store", {
								model : modelName,
								autoLoad : false,
								data : []
							}),
					listeners : {
						itemdblclick : {
							fn : me.onEditCompany,
							scope : me
						},
						select : {
							fn : me.onMainGridSelect,
							scope : me
						}
					}
				});

		return me.__mainGrid;
	},

	getPageItemGrid : function() {
		var me = this;
		if (me.__pageItemGrid) {
			return me.__pageItemGrid;
		}

		var modelName = "PSIExpressBillPageItem";
		Ext.define(modelName, {
					extend : "Ext.data.Model",
					fields : ["id", "caption", "left", "top", "width",
							"height", "fontSize", "isBold"]
				});

		me.__pageItemGrid = Ext.create("Ext.grid.Panel", {
					viewConfig : {
						enableTextSelection : true
					},
					columnLines : true,
					columns : [{
								xtype : "rownumberer"
							}, {
								header : "打印项",
								dataIndex : "caption",
								menuDisabled : true,
								sortable : false,
								width : 150
							}, {
								header : "左(毫米)",
								dataIndex : "left",
								menuDisabled : true,
								sortable : false
							}, {
								header : "上(毫米)",
								dataIndex : "top",
								menuDisabled : true,
								sortable : false
							}, {
								header : "宽度(毫米)",
								dataIndex : "width",
								menuDisabled : true,
								sortable : false
							}, {
								header : "高度(毫米)",
								dataIndex : "height",
								menuDisabled : true,
								sortable : false
							}, {
								header : "字体大小",
								dataIndex : "fontSize",
								menuDisabled : true,
								sortable : false
							}, {
								header : "字体加粗",
								dataIndex : "isBold",
								menuDisabled : true,
								sortable : false
							}],
					store : Ext.create("Ext.data.Store", {
								model : modelName,
								autoLoad : false,
								data : []
							}),
					listeners : {
						itemdblclick : {
							fn : me.onEditCompany,
							scope : me
						}
					}
				});

		return me.__pageItemGrid;
	},

	getPageCmp : function() {
		var me = this;

		return [{
					id : "editEcPageTop",
					labelWidth : 100,
					labelAlign : "right",
					labelSeparator : "",
					xtype : "textfield",
					margin : 5,
					readOnly : true,
					fieldLabel : "向下偏(毫米)"
				}, {
					id : "editEcPageLeft",
					labelWidth : 100,
					labelAlign : "right",
					labelSeparator : "",
					xtype : "textfield",
					margin : 5,
					readOnly : true,
					fieldLabel : "向右偏(毫米)"
				}, {
					id : "editEcPageWidth",
					labelWidth : 100,
					labelAlign : "right",
					labelSeparator : "",
					xtype : "textfield",
					margin : 5,
					readOnly : true,
					fieldLabel : "纸张宽度(毫米)"
				}, {
					id : "editEcPageHeight",
					labelWidth : 100,
					labelAlign : "right",
					labelSeparator : "",
					xtype : "textfield",
					margin : 5,
					readOnly : true,
					fieldLabel : "纸张高度(毫米)"
				}, {
					id : "editEcPageOrient",
					labelWidth : 100,
					labelAlign : "right",
					labelSeparator : "",
					xtype : "textfield",
					margin : 5,
					readOnly : true,
					fieldLabel : "打印方向"
				}, {
					xtype : "button",
					text : "设计打印样式",
					margin : "5 5 5 20",
					width : 120,
					iconCls : "PSI-button-edit",
					handler : me.onDesignBill,
					scope : me
				}];
	},

	onMainGridSelect : function() {
		this.refreshTemplateInfo();
	},

	refreshTemplateInfo : function() {
		var me = this;
		var item = me.getMainGrid().getSelectionModel().getSelection();
		if (item == null || item.length != 1) {
			return;
		}

		var express = item[0];

		var grid = me.getPageItemGrid();
		var el = grid.getEl() || Ext.getBody();
		el.mask(PSI.Const.LOADING);
		var r = {
			url : PSI.Const.BASE_URL + "Home/Express/getTemplateInfo",
			params : {
				id : express.get("id")
			},
			method : "POST",
			callback : function(options, success, response) {
				el.unmask();

				if (!success) {
					PSI.MsgBox.showInfo("网络错误");
					return;
				}

				var data = Ext.JSON.decode(response.responseText);

				var store = me.getPageItemGrid().getStore();
				store.removeAll();

				if (data.hasData) {
					var v = data.pageInfo;
					Ext.getCmp("editEcPageLeft").setValue(v.pageLeft);
					Ext.getCmp("editEcPageTop").setValue(v.pageTop);
					Ext.getCmp("editEcPageWidth").setValue(v.pageWidth);
					Ext.getCmp("editEcPageHeight").setValue(v.pageHeight);
					Ext.getCmp("editEcPageOrient").setValue(v.pageOrient);
					store.add(data.items);
				} else {
					Ext.getCmp("editEcPageLeft").setValue(null);
					Ext.getCmp("editEcPageTop").setValue(null);
					Ext.getCmp("editEcPageWidth").setValue(null);
					Ext.getCmp("editEcPageHeight").setValue(null);
					Ext.getCmp("editEcPageOrient").setValue(null);
				}
			}
		};

		Ext.Ajax.request(r);
	}
});