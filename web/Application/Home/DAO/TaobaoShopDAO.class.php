<?php

namespace Home\DAO;

use Home\Service\IdGenService;
use Home\Service\UserService;

/**
 * 运单 DAO
 *
 * @author 李静波
 */
class TaobaoShopDAO extends PSIBaseDAO {
	private $LOG_CATEGORY = "淘宝店铺管理";

	/**
	 * 淘宝店铺列表
	 */
	public function shopList() {
		$db = M();
		
		$sql = "select id, name from t_taobao_shop";
		
		return $db->query($sql);
	}

	/**
	 * 新增或编辑淘宝店铺
	 */
	public function editShop($params) {
		$id = $params["id"];
		$name = $params["name"];
		
		$us = new UserService();
		$companyId = $us->getCompanyId();
		$dataOrg = $us->getLoginUserDataOrg();
		
		$db = M();
		
		$db->startTrans();
		
		$log = null;
		
		if ($id) {
			// 编辑
			// 检查该名称的店铺是否已经存在
			$sql = "select count(*) as cnt from t_taobao_shop 
					where name = '%s' and id <> '%s' ";
			$data = $db->query($sql, $name, $id);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("店铺[$name]已经存在");
			}
			
			$sql = "update t_taobao_shop
					set name = '%s'
					where id = '%s' ";
			$rc = $db->execute($sql, $name, $id);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
			
			$log = "编辑淘宝店铺[$name]";
		} else {
			// 新增
			$idGen = new IdGenService();
			$id = $idGen->newId($db);
			
			// 检查该名称的店铺是否已经存在
			$sql = "select count(*) as cnt from t_taobao_shop where name = '%s' ";
			$data = $db->query($sql, $name);
			$cnt = $data[0]["cnt"];
			if ($cnt > 0) {
				$db->rollback();
				return $this->bad("店铺[$name]已经存在");
			}
			
			// 暂时没有使用code字段
			$code = "";
			$sql = "insert into t_taobao_shop (id, code, name, data_org, company_id)
					values ('%s', '%s', '%s', '%s', '%s')";
			$rc = $db->execute($sql, $id, $code, $name, $dataOrg, $companyId);
			if ($rc === false) {
				$db->rollback();
				return $this->sqlError(__LINE__);
			}
			
			$log = "新增淘宝店铺[$name]";
		}
		
		// 记录业务日志
		if ($log) {
			$bzDAO = new BizlogDAO($db);
			$bzDAO->insertBizlog($log, $this->LOG_CATEGORY);
		}
		
		$db->commit();
		
		return $this->ok($id);
	}

	/**
	 * 删除淘宝店铺
	 */
	public function deleteShop($params) {
		$id = $params["id"];
		
		$db = M();
		$db->startTrans();
		$sql = "select name from t_taobao_shop where id = '%s' ";
		$data = $db->query($sql, $id);
		if (! $data) {
			$db->rollback();
			return $this->bad("要删除的淘宝店铺不存在");
		}
		$name = $data[0]["name"];
		
		// 检查该淘宝店铺是否被使用了
		$sql = "select count(*) as cnt from t_taobao_goods where shop_id = '%s' ";
		$data = $db->query($sql, $id);
		$cnt = $data[0]["cnt"];
		if ($cnt > 0) {
			$db->rollback();
			return $this->bad("淘宝店铺[$name]里面还有商品数据，不能删除");
		}
		
		$sql = "delete from t_taobao_shop where id = '%s' ";
		$rc = $db->execute($sql, $id);
		if ($rc === false) {
			$db->rollback();
			return $this->sqlError(__LINE__);
		}
		
		$log = "删除淘宝店铺[$name]";
		$blDAO = new BizlogDAO($db);
		$blDAO->insertBizlog($log, $this->LOG_CATEGORY);
		
		$db->commit();
		
		return $this->ok();
	}
}