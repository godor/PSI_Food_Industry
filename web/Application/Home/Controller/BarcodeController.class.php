<?php

namespace Home\Controller;

use Home\Common\FIdConst;
use Home\Service\UserService;
use Think\Controller;
use Home\Service\BarcodeService;

/**
 * 条码库 Controller
 *
 * @author 李静波
 *        
 */
class BarcodeController extends PSIBaseController {

	/**
	 * 条码库 - 主页面
	 */
	public function index() {
		$us = new UserService();
		
		if ($us->hasPermission(FIdConst::BARCODE_LIB)) {
			$this->initVar();
			
			$this->assign("title", "条码库");
			
			$this->display();
		} else {
			$this->gotoLoginPage("/Home/Barcode/index");
		}
	}

	public function editBarcode() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id"),
					"code" => I("post.code")
			);
			
			$bs = new BarcodeService();
			
			$this->ajaxReturn($bs->editBarcode($params));
		}
	}

	public function barcodeList() {
		if (IS_POST) {
			$params = array(
					"barcode" => I("post.barcode"),
					"goodsId" => I("post.goodsId"),
					"page" => I("post.page"),
					"start" => I("post.start"),
					"limit" => I("post.limit")
			);
			
			$bs = new BarcodeService();
			
			$this->ajaxReturn($bs->barcodeList($params));
		}
	}

	public function goodsList() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$bs = new BarcodeService();
			
			$this->ajaxReturn($bs->goodsList($params));
		}
	}

	public function deleteBarcode() {
		if (IS_POST) {
			$params = array(
					"id" => I("post.id")
			);
			
			$bs = new BarcodeService();
			
			$this->ajaxReturn($bs->deleteBarcode($params));
		}
	}

	/**
	 * 条码自定义字段，查询数据
	 */
	public function queryData() {
		if (IS_POST) {
			$queryKey = I("post.queryKey");
			$bs = new BarcodeService();
			
			$this->ajaxReturn($bs->queryData($queryKey));
		}
	}
}