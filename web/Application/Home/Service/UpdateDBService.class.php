<?php

namespace Home\Service;

use Home\Common\FIdConst;

/**
 * 数据库升级Service
 *
 * @author 李静波
 */
class UpdateDBService extends PSIBaseService {

	public function updateDatabase() {
		if ($this->isNotOnline()) {
			return $this->notOnlineError();
		}
		
		$db = M();
		
		// 检查t_psi_db_version中的版本号
		$sql = "select db_version from t_psi_db_version";
		$data = $db->query($sql);
		$dbVersion = $data[0]["db_version"];
		if ($dbVersion == $this->CURRENT_DB_VERSION) {
			return $this->bad("当前数据库是最新版本，不用升级");
		}
		
		// $this->update_20160708_01($db);
		// $this->update_20160710_01($db);
		// $this->update_20160711_01($db);
		// $this->update_20160712_01($db);
		// $this->update_20160712_02($db);
		// $this->update_20160714_01($db);
		// $this->update_20160715_01($db);
		// $this->update_20160715_02($db);
		// $this->update_20160715_03($db);
		// $this->update_20160719_01($db);
		// $this->update_20160719_02($db);
		// $this->update_20160720_01($db);
		// $this->update_20160721_01($db);
		// $this->update_20160722_01($db);
		// $this->update_20160723_01($db);
		// $this->update_20160723_02($db);
		// $this->update_20160725_01($db);
		// $this->update_20160726_01($db);
		// $this->update_20160726_02($db);
		// $this->update_20160727_01($db);
		// $this->update_20160728_01($db);
		// $this->update_20160729_01($db);
		// $this->update_20160729_02($db);
		// $this->update_20160801_01($db);
		// $this->update_20160801_02($db);
		// $this->update_20160801_03($db);
		// $this->update_20160801_04($db);
		// $this->update_20160803_01($db);
		// $this->update_20160803_02($db);
		// $this->update_20160804_01($db);
		
		// $this->update_20160814_01($db);
		// $this->update_20160816_01($db);
		// $this->update_20160817_01($db);
		// $this->update_20160821_01($db);
		
		// $this->update_20160825_01($db);
		
		// $this->update_20160827_01($db);
		
		// $this->update_20160831_01($db);
		// $this->update_20160901_01($db);
		
		// $this->update_20160902_01($db);
		// $this->update_20160902_02($db);
		
		// $this->update_20160905_01($db);
		
		// $this->update_20160910_01($db);
		
		// $this->update_20160912_01($db);
		
		// $this->update_20160913_01($db);
		
		// $this->update_20160914_01($db);
		
		// $this->update_20160917_01($db);
		
		// $this->update_20160921_01($db);
		
		// $this->update_20160923_01($db);
		
		// $this->update_20160923_02($db);
		
		// $this->update_20160923_03($db);
		
		// $this->update_20160924_01($db);
		
		// $this->update_20160924_02($db);
		
		// $this->update_20161024_01($db);
		
		// $this->update_20161114_01($db);
		
		$this->update_20161118_01($db);
		
		$sql = "delete from t_psi_db_version";
		$db->execute($sql);
		$sql = "insert into t_psi_db_version (db_version, update_dt) 
				values ('%s', now())";
		$db->execute($sql, $this->CURRENT_DB_VERSION);
		
		$bl = new BizlogService();
		$bl->insertBizlog("升级数据库，数据库版本 = " . $this->CURRENT_DB_VERSION);
		
		return $this->ok();
	}

	private function update_20161118_01($db) {
		// 本次更新：新增模块 报损报溢
		$fid = FIdConst::LOSS_OR_OVER_MANAGEMNET;
		$name = "报损报溢";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "报损报溢";
			$note = "通过菜单进入报损报溢模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '0507' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('0507', '%s', '%s', '05', 6)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20161114_01($db) {
		// 本次更新：新增表 t_lo_bill t_lo_bill_detail
		$talbeName = "t_lo_bill";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_lo_bill` (
					  `id` varchar(255) NOT NULL,
					  `bill_status` int(11) NOT NULL,
					  `bizdt` datetime NOT NULL,
					  `biz_user_id` varchar(255) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `input_user_id` varchar(255) NOT NULL,
					  `ref` varchar(255) NOT NULL,
					  `warehouse_id` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  `icbill_id` varchar(255) DEFAULT NULL,
					  `icbill_ref` varchar(255) DEFAULT NULL,
					  `confirm_user_id` varchar(255) DEFAULT NULL,
					  `confirm_dt` datetime DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_lo_bill_detail";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_lo_bill_detail` (
					  `id` varchar(255) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `goods_count` int(11) NOT NULL,
					  `goods_price` decimal(19,2) DEFAULT NULL,
					  `goods_money` decimal(19,2) DEFAULT NULL,
					  `show_order` int(11) NOT NULL,
					  `lobill_id` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  `qc_begin_dt` datetime DEFAULT NULL,
					  `qc_end_dt` datetime DEFAULT NULL,
					  `expiration` int(11) DEFAULT NULL,
					  `loss_or_over` int(11) NOT NULL,
					  `note` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
	}

	private function update_20161024_01($db) {
		// 本次更新：新增模块销售订单跟踪表、删除原有报表
		$fid = FIdConst::REPORT_SALES_ORDER_LIST;
		$name = "销售订单跟踪表";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售报表";
			$note = "通过菜单进入销售订单跟踪表的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 菜单
		$sql = "TRUNCATE TABLE `t_menu_item`;";
		$db->execute($sql);
		$sql = "INSERT INTO `t_menu_item` (`id`, `caption`, `fid`, `parent_id`, `show_order`) VALUES
				('01', '基础信息', NULL, NULL, 1),
				('0101', '客户资料', '1007', '01', 1),
				('0102', '快递公司', '2036', '01', 2),
				('0103', '销售收入方式', '2035', '01', 3),
				('02', '商品', NULL, NULL, 2),
				('0201', '商品', '1001', '02', 1),
				('0202', '商品计量单位', '1002', '02', 2),
				('0203', '商品品牌', '2029', '02', 3),
				('0204', '价格体系', '2034', '02', 4),
				('0205', '条码库', '2040', '02', 5),
				('03', '销售', NULL, NULL, 3),
				('0301', '销售订单', '2028', '03', 1),
				('0302', '销售出库', '2002', '03', 2),
				('0303', '收银系统', '2039', '03', 3),
				('04', '售后', NULL, NULL, 4),
				('0401', '售后服务', '2037', '04', 1),
				('0402', '销售退货入库', '2006', '04', 2),
				('05', '仓库', NULL, NULL, 5),
				('0501', '仓库', '1003', '05', 1),
				('0502', '库存建账', '2000', '05', 2),
				('0503', '库存账查询', '2003', '05', 3),
				('0504', '库间调拨', '2009', '05', 4),
				('0506', '库存盘点', '2010', '05', 5),
				('06', '采购', NULL, NULL, 6),
				('0601', '供应商档案', '1004', '06', 1),
				('0602', '采购订单', '2027', '06', 2),
				('0603', '采购入库', '2001', '06', 3),
				('0604', '采购退货出库', '2007', '06', 4),
				('07', '财务', NULL, NULL, 7),
				('0701', '应收账款管理', '2004', '07', 1),
				('0702', '应付账款管理', '2005', '07', 2),
				('0703', '现金收支查询', '2024', '07', 3),
				('0704', '预收款管理', '2025', '07', 4),
				('0705', '预付款管理', '2026', '07', 5),
				('08', '人事', NULL, NULL, 8),
				('0801', '用户管理', '-8999', '08', 1),
				('0802', '人员职务', '2030', '08', 2),
				('09', '报表', NULL, NULL, 9),
				('0901', '销售报表', NULL, '09', 1),
				('090101', '销售订单跟踪表', '2041', '0901', 1),
				('10', '系统', NULL, NULL, 10),
				('1001', '修改我的密码', '-9996', '10', 1),
				('1002', '重新登录', '-9999', '10', 2),
				('1003', '权限管理', '-8996', '10', 3),
				('1004', '业务日志', '-8997', '10', 4),
				('1005', '业务设置', '2008', '10', 5),
				('1006', '店铺管理', '2038', '10', 6),
				('11', '帮助', NULL, NULL, 11),
				('1103', '关于', '-9994', '11', 3);
				";
		$db->execute($sql);
		
		// 删除旧报表的权限
		$oldFIdList = array(
				"2012",
				"2013",
				"2014",
				"2015",
				"2016",
				"2017",
				"2018",
				"2019",
				"2020",
				"2021",
				"2022",
				"2023"
		);
		
		foreach ( $oldFIdList as $fid ) {
			$sql = "delete from t_fid where fid = '%s' ";
			$db->execute($sql, $fid);
			
			$sql = "delete from t_permission where id = '%s' ";
			$db->execute($sql, $fid);
		}
	}

	private function update_20160924_02($db) {
		// 本次更新： 迁移旧条码数据到新数据
		if ($this->CURRENT_DB_VERSION != "20160924-02") {
			return;
		}
		
		$idGen = new IdGenService();
		
		$sql = "select id, bar_code
				from t_goods
				where bar_code is not null and bar_code <> '' ";
		$data = $db->query($sql);
		foreach ( $data as $v ) {
			$goodsId = $v["id"];
			$barcode = $v["bar_code"];
			
			$barcodeId = null;
			
			$sql = "select id from t_barcode where barcode = '%s' ";
			$d = $db->query($sql, $barcode);
			if ($d) {
				$barcodeId = $d[0]["id"];
			} else {
				$barcodeId = $idGen->newId($db);
				
				$sql = "insert into t_barcode(id, barcode)
						values ('%s', '%s')";
				$db->execute($sql, $barcodeId, $barcode);
			}
			
			$sql = "select id from t_goods_barcode 
					where goods_id = '%s' and barcode_id = '%s' ";
			$d = $db->query($sql, $goodsId, $barcodeId);
			if ($d) {
				continue;
			}
			
			$sql = "insert into t_goods_barcode(id, goods_id, barcode_id)
					values ('%s', '%s', '%s')";
			$db->execute($sql, $idGen->newId($db), $goodsId, $barcodeId);
		}
	}

	private function update_20160924_01($db) {
		// 本次更新：t_goods表新增字段package_count
		$tableName = "t_goods";
		$columnName = "package_count";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19,2) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160923_03($db) {
		// 本次更新：新增模块 条码库
		$fid = FIdConst::BARCODE_LIB;
		$name = "条码库";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "条码库";
			$note = "通过菜单进入条码库模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '0205' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('0205', '%s', '%s', '02', 5)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20160923_02($db) {
		// 本次更新：新增表 t_barcode t_goods_barcode
		$talbeName = "t_barcode";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_barcode` (
					  `id` varchar(255) NOT NULL,
					  `barcode` varchar(255) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_goods_barcode";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_goods_barcode` (
					  `id` varchar(255) NOT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `barcode_id` varchar(255) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
	}

	private function update_20160923_01($db) {
		// 本次更新：新增模块 收银系统
		$fid = FIdConst::POS;
		$name = "收银系统";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "收银系统";
			$note = "通过菜单进入收银系统模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '0303' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('0303', '%s', '%s', '03', 3)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20160921_01($db) {
		// 本次更新：t_as_bill新增字段 gen_srbill_id gen_srbill_ref gen_sobill_id gen_sobill_ref
		$tableName = "t_as_bill";
		$columnName = "gen_srbill_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		$columnName = "gen_srbill_ref";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		$columnName = "gen_sobill_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		$columnName = "gen_sobill_ref";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160917_01($db) {
		// 本次更新：新增表 t_pos_bill t_pos_bill_detail
		$talbeName = "t_pos_bill";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_pos_bill` (
					  `id` varchar(255) NOT NULL,
					  `bill_status` int(11) NOT NULL,
					  `bizdt` datetime NOT NULL,
					  `biz_user_id` varchar(255) NOT NULL,
					  `customer_id` varchar(255) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `input_user_id` varchar(255) NOT NULL,
					  `inventory_money` decimal(19,2) DEFAULT NULL,
					  `profit` decimal(19,2) DEFAULT NULL,
					  `ref` varchar(255) NOT NULL,
					  `sale_money` decimal(19,2) DEFAULT NULL,
					  `warehouse_id` varchar(255) NOT NULL,
					  `receiving_type` int(11) NOT NULL DEFAULT 0,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  `memo` varchar(1000) DEFAULT NULL,
					  `shop_id` varchar(255) NOT NULL,
					  `total_pay` decimal(19,2) DEFAULT NULL,
					  `return_money` decimal(19,2) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_pos_bill_detail";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_pos_bill_detail` (
					  `id` varchar(255) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `goods_count` int(11) NOT NULL,
					  `goods_money` decimal(19,2) NOT NULL,
					  `goods_price` decimal(19,2) NOT NULL,
					  `inventory_money` decimal(19,2) DEFAULT NULL,
					  `inventory_price` decimal(19,2) DEFAULT NULL,
					  `show_order` int(11) NOT NULL,
					  `posbill_id` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
	}

	private function update_20160914_01($db) {
		// 本次更新：新增模块 店铺管理
		$fid = FIdConst::SHOP_MANAGEMENT;
		$name = "店铺管理";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "店铺管理";
			$note = "通过菜单进入店铺管理模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '1006' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('1006', '%s', '%s', '10', 6)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20160913_01($db) {
		// 本次更新：新增表 t_shop t_shop_user
		$talbeName = "t_shop";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_shop` (
					  `id` varchar(255) NOT NULL,
					  `code` varchar(255) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  `address` varchar(255) DEFAULT NULL,
					  `shop_master_id` varchar(255) NOT NULL,
					  `shop_type` int(11) NOT NULL DEFAULT 0,
					  `use_pos` int(11) NOT NULL DEFAULT  1,
					  `warehouse_id` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_shop_user";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_shop_user` (
					  `id` varchar(255) NOT NULL,
					  `shop_id` varchar(255) NOT NULL,
					  `user_id` varchar(255) NOT NULL,
					  `auto_goto_pos` int(11) NOT NULL DEFAULT  1,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
	}

	private function update_20160912_01($db) {
		// 本次更新：销售退货入库单新增保质期字段
		$tableName = "t_sr_bill_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160910_01($db) {
		// 本次更新：售后单新增保质期字段
		$tableName = "t_as_bill_rej_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
		
		$tableName = "t_as_bill_ex_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160905_01($db) {
		// 本次更新： t_as_bill新增字段 payment_type receiving_type sales_mode_id
		$tableName = "t_as_bill";
		$columnName = "payment_type";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default 0;";
			$db->execute($sql);
		}
		
		$columnName = "receiving_type";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default 0;";
			$db->execute($sql);
		}
		
		$columnName = "sales_mode_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) DEFAULT NULL;";
			$db->execute($sql);
		}
	}

	private function update_20160902_02($db) {
		// 本次更新：新增模块【售后服务】
		$fid = FIdConst::AFTER_SALES;
		$name = "售后服务";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "售后";
			$note = "通过菜单进入售后服务模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '0401' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('0401', '%s', '%s', '04', 1)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20160902_01($db) {
		// 本次更新：新增表 t_as_bill、t_as_bill_rej_detail、t_as_bill_ex_detail
		$talbeName = "t_as_bill";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_as_bill` (
					  `id` varchar(255) NOT NULL,
					  `ref` varchar(255) NOT NULL,
					  `customer_id` varchar(255) NOT NULL,
					  `in_warehouse_id` varchar(255) NOT NULL,
					  `out_warehouse_id` varchar(255) NOT NULL,
					  `biz_user_id` varchar(255) NOT NULL,
					  `bizdt` datetime NOT NULL,
					  `input_user_id` varchar(255) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `bill_status` int(11) NOT NULL,
					  `biz_type` int(11) NOT NULL,
					  `srv_summary` varchar(255) NOT NULL,
					  `srv_note` varchar(1000) DEFAULT NULL,
					  `confirm_user_id` varchar(255) NOT NULL,
					  `confirm_dt` datetime DEFAULT NULL,
					  `rej_money` decimal(19,2) DEFAULT NULL,
					  `sobill_id` varchar(255) NOT NULL,
					  `sobill_ref` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_as_bill_rej_detail";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_as_bill_rej_detail` (
					  `id` varchar(255) NOT NULL,
					  `asbill_id` varchar(255) NOT NULL,
					  `show_order` int(11) NOT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `goods_count` int(11) NOT NULL,
					  `goods_money` decimal(19,2) NOT NULL,
					  `goods_price` decimal(19,2) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `inventory_price` decimal(19,2) NOT NULL,
					  `inventory_money` decimal(19,2) NOT NULL,
					  `wsbilldetail_id` varchar(255) NOT NULL,
					  `rejection_goods_count` int(11) NOT NULL,
					  `rejection_goods_price` decimal(19,2) NOT NULL,
					  `rejection_sale_money` decimal(19,2) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_as_bill_ex_detail";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_as_bill_ex_detail` (
					  `id` varchar(255) NOT NULL,
					  `asbill_id` varchar(255) NOT NULL,
					  `show_order` int(11) NOT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `goods_count` int(11) NOT NULL,
					  `goods_money` decimal(19,2) NOT NULL,
					  `goods_price` decimal(19,2) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
	}

	private function update_20160901_01($db) {
		// 本次更新： 重新调整菜单
		if ($this->CURRENT_DB_VERSION != "20160901-01") {
			return;
		}
		
		$sql = "TRUNCATE TABLE `t_menu_item`;";
		$db->execute($sql);
		
		$sql = "INSERT INTO `t_menu_item` (`id`, `caption`, `fid`, `parent_id`, `show_order`) VALUES
				('01', '基础信息', NULL, NULL, 1),
				('0101', '客户资料', '1007', '01', 1),
				('0102', '快递公司', '2036', '01', 2),
				('0103', '销售收入方式', '2035', '01', 3),
				('02', '商品', NULL, NULL, 2),
				('0201', '商品', '1001', '02', 1),
				('0202', '商品计量单位', '1002', '02', 2),
				('0203', '商品品牌', '2029', '02', 3),
				('0204', '价格体系', '2034', '02', 4),
				('03', '销售', NULL, NULL, 3),
				('0301', '销售订单', '2028', '03', 1),
				('0302', '销售出库', '2002', '03', 2),
				('04', '售后', NULL, NULL, 4),
				('0402', '销售退货入库', '2006', '04', 2),
				('05', '仓库', NULL, NULL, 5),
				('0501', '仓库', '1003', '05', 1),
				('0502', '库存建账', '2000', '05', 2),
				('0503', '库存账查询', '2003', '05', 3),
				('0504', '库间调拨', '2009', '05', 4),
				('0506', '库存盘点', '2010', '05', 5),
				('06', '采购', NULL, NULL, 6),
				('0601', '供应商档案', '1004', '06', 1),
				('0602', '采购订单', '2027', '06', 2),
				('0603', '采购入库', '2001', '06', 3),
				('0604', '采购退货出库', '2007', '06', 4),
				('07', '财务', NULL, NULL, 7),
				('0701', '应收账款管理', '2004', '07', 1),
				('0702', '应付账款管理', '2005', '07', 2),
				('0703', '现金收支查询', '2024', '07', 3),
				('0704', '预收款管理', '2025', '07', 4),
				('0705', '预付款管理', '2026', '07', 5),
				('08', '人事', NULL, NULL, 8),
				('0801', '用户管理', '-8999', '08', 1),
				('0802', '人员职务', '2030', '08', 2),
				('09', '报表', NULL, NULL, 9),
				('0901', '销售日报表', NULL, '09', 1),
				('090101', '销售日报表(按商品汇总)', '2012', '0901', 1),
				('090102', '销售日报表(按客户汇总)', '2013', '0901', 2),
				('090103', '销售日报表(按仓库汇总)', '2014', '0901', 3),
				('090104', '销售日报表(按业务员汇总)', '2015', '0901', 4),
				('0902', '销售月报表', NULL, '09', 2),
				('090201', '销售月报表(按商品汇总)', '2016', '0902', 1),
				('090202', '销售月报表(按客户汇总)', '2017', '0902', 2),
				('090203', '销售月报表(按仓库汇总)', '2018', '0902', 3),
				('090204', '销售月报表(按业务员汇总)', '2019', '0902', 4),
				('0906', '资金报表', NULL, '09', 6),
				('090601', '应收账款账龄分析表', '2021', '0906', 1),
				('090602', '应付账款账龄分析表', '2022', '0906', 2),
				('10', '系统', NULL, NULL, 10),
				('1001', '修改我的密码', '-9996', '10', 1),
				('1002', '重新登录', '-9999', '10', 2),
				('1003', '权限管理', '-8996', '10', 3),
				('1004', '业务日志', '-8997', '10', 4),
				('1005', '业务设置', '2008', '10', 5);
				";
		$db->execute($sql);
	}

	private function update_20160831_01($db) {
		// 本次更新：t_goods表新增字段 use_qc
		$tableName = "t_goods";
		$columnName = "use_qc";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default 0;";
			$db->execute($sql);
		}
	}

	private function update_20160827_01($db) {
		// 本次更新： t_goods_brand新增字段 py
		$tableName = "t_goods_brand";
		$columnName = "py";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		// 更新py字段的值
		$sql = "select id, full_name from t_goods_brand";
		$data = $db->query($sql);
		$ps = new PinyinService();
		foreach ( $data as $v ) {
			$id = $v["id"];
			$name = $v["full_name"];
			$py = $ps->toPY($name);
			$sql = "update t_goods_brand
					set py = '%s'
					where id = '%s' ";
			$db->execute($sql, $py, $id);
		}
	}

	private function update_20160825_01($db) {
		// 本次更新：t_so_bill新增字段 deal_province deal_city deal_district
		$tableName = "t_so_bill";
		$columnName = "deal_province";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		$columnName = "deal_city";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		$columnName = "deal_district";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160821_01($db) {
		// 本次更新：t_express_bill_printitem新增字段is_bold
		$tableName = "t_express_bill_printitem";
		$columnName = "is_bold";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default 0;";
			$db->execute($sql);
		}
	}

	private function update_20160817_01($db) {
		// 本次更新 销售订单新增折扣字段
		$tableName = "t_so_bill";
		$columnName = "discount";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default 100;";
			$db->execute($sql);
		}
		
		$tableName = "t_so_bill_detail";
		$columnName = "discount";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default 100;";
			$db->execute($sql);
		}
		$columnName = "money_before_discount";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19,2) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160816_01($db) {
		// 本次更新：t_it_bill新增字段ps_id
		$tableName = "t_it_bill";
		$columnName = "ps_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160814_01($db) {
		// 本次更新：新增表 t_express_bill_printpage t_express_bill_printitem
		$talbeName = "t_express_bill_printpage";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_express_bill_printpage` (
					  `id` varchar(255) NOT NULL,
					  `express_company_id` varchar(255) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  `page_top` decimal(19,2) NOT NULL,
					  `page_left` decimal(19,2) NOT NULL,
					  `page_width` decimal(19,2) NOT NULL,
					  `page_height` decimal(19,2) NOT NULL,
					  `page_orient` int(11) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_express_bill_printitem";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_express_bill_printitem` (
					  `id` varchar(255) NOT NULL,
					  `page_id` varchar(255) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  `caption` varchar(255) NOT NULL,
					  `item_top` decimal(19,2) NOT NULL,
					  `item_left` decimal(19,2) NOT NULL,
					  `item_width` decimal(19,2) NOT NULL,
					  `item_height` decimal(19,2) NOT NULL,
					  `font_size` int(11) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
	}

	private function update_20160804_01($db) {
		// 本次更新：新增省、市、县数据字典表
		$talbeName = "t_province";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_province` (
					  `id` varchar(255) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_city";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_city` (
					  `id` varchar(255) NOT NULL,
					  `province_id` varchar(255) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		$talbeName = "t_district";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_district` (
					  `id` varchar(255) NOT NULL,
					  `city_id` varchar(255) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
	}

	private function update_20160803_02($db) {
		// 本次更新：t_it_bill_detail新增qc_begin_dt qc_end_dt expiration字段
		$tableName = "t_it_bill_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160803_01($db) {
		// 本次更新：t_so_bill新增字段 print_count, last_print_dt
		$tableName = "t_so_bill";
		$columnName = "print_count";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) not null default 0;";
			$db->execute($sql);
		}
		
		$columnName = "last_print_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
	}

	private function update_20160801_04($db) {
		// 本次更新：新增模块 快递公司
		$fid = FIdConst::EXPRESS_COMPANY;
		$name = "快递公司";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售订单";
			$note = "通过菜单进入快递公司模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '0908' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('0908', '%s', '%s', '09', 8)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20160801_03($db) {
		// 本次更新：新增表t_express_comnapy;
		// t_so_bill新增字段sales_mode_id, express_company_id, freight
		// t_so_bill_detail新增字段 fregith
		//
		$talbeName = "t_express_company";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_express_company` (
					  `id` varchar(255) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  `py` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
					";
			$db->execute($sql);
		}
		
		$tableName = "t_so_bill";
		$columnName = "sales_mode_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		$columnName = "express_company_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		$columnName = "freight";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19, 2) default 0;";
			$db->execute($sql);
		}
		
		$tableName = "t_so_bill_detail";
		$columnName = "freight";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19, 2) default 0;";
			$db->execute($sql);
		}
	}

	private function update_20160801_02($db) {
		// 本次更新： 新增销售收入方式模块
		$fid = FIdConst::SALES_MODE;
		$name = "销售收入方式";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售订单";
			$note = "通过菜单进入销售收入方式模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '0907' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('0907', '%s', '%s', '09', 7)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20160801_01($db) {
		// 本次更新：新增表t_sales_mode
		$talbeName = "t_sales_mode";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_sales_mode` (
					  `id` varchar(255) NOT NULL,
					  `mode_type` int(11) NOT NULL,
					  `name` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
					";
			$db->execute($sql);
		}
	}

	private function update_20160729_02($db) {
		// 本次更新：t_ic_bill新增字段bill_type
		$tableName = "t_ic_bill";
		$columnName = "bill_type";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) not null default 0;";
			$db->execute($sql);
		}
	}

	private function update_20160729_01($db) {
		// 本次更新：盘点单新增保质期
		$tableName = "t_ic_bill_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160728_01($db) {
		// 本次更新：销售出库单权限细化到按钮
		
		// 新建销售出库单
		$fid = FIdConst::WAREHOUSING_SALE_ADD;
		$name = "销售出库 - 新建销售出库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售出库";
			$note = "销售出库[新建销售出库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 编辑销售出库单
		$fid = FIdConst::WAREHOUSING_SALE_EDIT;
		$name = "销售出库 - 编辑销售出库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售出库";
			$note = "销售出库[编辑销售出库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 删除销售出库单
		$fid = FIdConst::WAREHOUSING_SALE_DELETE;
		$name = "销售出库 - 删除销售出库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售出库";
			$note = "销售出库[删除销售出库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 提交销售出库单
		$fid = FIdConst::WAREHOUSING_SALE_COMMIT;
		$name = "销售出库 - 提交销售出库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售出库";
			$note = "销售出库[提交销售出库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 生成PDF
		$fid = FIdConst::WAREHOUSING_SALE_PDF;
		$name = "销售出库 - 生成PDF";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "销售出库";
			$note = "销售出库[生成PDF]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
	}

	private function update_20160727_01($db) {
		// 本次更新：t_so_bill新增字段 warehouse_id
		$tableName = "t_so_bill";
		$columnName = "warehouse_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160726_02($db) {
		// 本次更新：新增表t_customer_saleprice
		$talbeName = "t_customer_saleprice";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_customer_saleprice` (
					  `id` varchar(255) NOT NULL,
					  `customer_id` varchar(255) NOT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `sale_price` decimal(19,2) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;
					";
			$db->execute($sql);
		}
	}

	private function update_20160726_01($db) {
		// 本次更新：t_so_bill_detail新增字段 qc_begin_dt、qc_end_dt、expiration
		$tableName = "t_so_bill_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160725_01($db) {
		// 本次更新：t_goods表新增字段 disabled
		$tableName = "t_goods";
		$columnName = "disabled";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default 0;";
			$db->execute($sql);
		}
	}

	private function update_20160723_02($db) {
		// 本次更新：销售出库单新增保质期字段
		$tableName = "t_ws_bill_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160723_01($db) {
		// 本次更新：采购退货出库单新增保质期字段
		$tableName = "t_pr_bill_detail";
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160722_01($db) {
		// 本次更新：新增表t_inventory_lot
		$tableName = "t_inventory_lot";
		
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_inventory_lot` (
					  `id` bigint(20) NOT NULL AUTO_INCREMENT,
					  `balance_count` decimal(19,2) NOT NULL,
					  `balance_money` decimal(19,2) NOT NULL,
					  `balance_price` decimal(19,2) NOT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `in_count` decimal(19,2) DEFAULT NULL,
					  `in_money` decimal(19,2) DEFAULT NULL,
					  `in_price` decimal(19,2) DEFAULT NULL,
					  `out_count` decimal(19,2) DEFAULT NULL,
					  `out_money` decimal(19,2) DEFAULT NULL,
					  `out_price` decimal(19,2) DEFAULT NULL,
					  `afloat_count` decimal(19,2) DEFAULT NULL,
					  `afloat_money` decimal(19,2) DEFAULT NULL,
					  `afloat_price` decimal(19,2) DEFAULT NULL,
					  `warehouse_id` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  `begin_dt` datetime NOT NULL,
					  `end_dt` datetime NOT NULL,
					  `expiration` int(11) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
					";
			$db->execute($sql);
		}
	}

	private function update_20160721_01($db) {
		// 本次更新：采购入库权限细化到按钮
		
		// 新建采购入库单
		$fid = FIdConst::PURCHASE_WAREHOUSE_ADD;
		$name = "采购入库 - 新建采购入库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "采购入库";
			$note = "采购入库[新建采购入库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 编辑采购入库单
		$fid = FIdConst::PURCHASE_WAREHOUSE_EDIT;
		$name = "采购入库 - 编辑采购入库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "采购入库";
			$note = "采购入库[编辑采购入库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 删除采购入库单
		$fid = FIdConst::PURCHASE_WAREHOUSE_DELETE;
		$name = "采购入库 - 删除采购入库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "采购入库";
			$note = "采购入库[删除采购入库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		// 提交采购入库单
		$fid = FIdConst::PURCHASE_WAREHOUSE_COMMIT;
		$name = "采购入库 - 提交采购入库单";
		
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "采购入库";
			$note = "采购入库[提交采购入库单]按钮的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
	}

	private function update_20160720_01($db) {
		// 本次更新：采购入库单新增 运费、保质期
		$tableName = "t_pw_bill";
		$columnName = "freight";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19, 2) default null;";
			$db->execute($sql);
		}
		
		// 明细表
		$tableName = "t_pw_bill_detail";
		$columnName = "freight";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19, 2) default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160719_02($db) {
		// 本次更新：采购订单新增 运费、保质期
		$tableName = "t_po_bill";
		$columnName = "freight";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19, 2) default null;";
			$db->execute($sql);
		}
		
		// 明细表
		$tableName = "t_po_bill_detail";
		$columnName = "freight";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19, 2) default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_begin_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "qc_end_dt";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} datetime default null;";
			$db->execute($sql);
		}
		
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int default null;";
			$db->execute($sql);
		}
	}

	private function update_20160719_01($db) {
		// 本次更新：新增表t_inventory_detail_lot
		$tableName = "t_inventory_detail_lot";
		
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_inventory_detail_lot` (
					  `id` bigint(20) NOT NULL AUTO_INCREMENT,
					  `balance_count` decimal(19,2) NOT NULL,
					  `balance_money` decimal(19,2) NOT NULL,
					  `balance_price` decimal(19,2) NOT NULL,
					  `biz_date` datetime NOT NULL,
					  `biz_user_id` varchar(255) NOT NULL,
					  `date_created` datetime DEFAULT NULL,
					  `goods_id` varchar(255) NOT NULL,
					  `in_count` decimal(19,2) DEFAULT NULL,
					  `in_money` decimal(19,2) DEFAULT NULL,
					  `in_price` decimal(19,2) DEFAULT NULL,
					  `out_count` decimal(19,2) DEFAULT NULL,
					  `out_money` decimal(19,2) DEFAULT NULL,
					  `out_price` decimal(19,2) DEFAULT NULL,
					  `ref_number` varchar(255) DEFAULT NULL,
					  `ref_type` varchar(255) NOT NULL,
					  `warehouse_id` varchar(255) NOT NULL,
					  `data_org` varchar(255) DEFAULT NULL,
					  `company_id` varchar(255) DEFAULT NULL,
					  `begin_dt` datetime NOT NULL,
					  `end_dt` datetime NOT NULL,
					  `expiration` int(11) NOT NULL,
					  PRIMARY KEY (`id`)
					) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
					";
			$db->execute($sql);
		}
	}

	private function update_20160715_03($db) {
		// 本次更新 t_supplier新增字段province、city、district、postcode、email01、email02
		$tableName = "t_supplier";
		$columnName = "province";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "city";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "district";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "postcode";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "email01";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "email02";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160715_02($db) {
		// 本次更新： t_customer表新增字段 email01、email02
		$tableName = "t_customer";
		$columnName = "email01";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "email02";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160715_01($db) {
		// 本次更新：t_customer表新增字段 taobao_nickname、province、city、district、postcode
		$tableName = "t_customer";
		$columnName = "taobao_nickname";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "province";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "city";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "district";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		$columnName = "postcode";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160714_01($db) {
		// 本次更新：t_goods表新增字段 expiration、weight、manufacturer
		$tableName = "t_goods";
		$columnName = "expiration";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} int(11) default null;";
			$db->execute($sql);
		}
		
		$columnName = "weight";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19,2) default null;";
			$db->execute($sql);
		}
		
		$columnName = "manufacturer";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160712_02($db) {
		// 本次更新：t_goods表新增字段base_sale_price
		$tableName = "t_goods";
		$columnName = "base_sale_price";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19,2) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160712_01($db) {
		// 本次更新：t_price_system表新增字段 factor
		$tableName = "t_price_system";
		$columnName = "factor";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} decimal(19,2) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160711_01($db) {
		// 本次更新： 新增模块 价格体系
		$fid = FIdConst::PRICE_SYSTEM;
		$name = "价格体系";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "商品";
			$note = "通过菜单进入价格体系模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '090104' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('090104', '%s', '%s', '0901', 4)";
			$db->execute($sql, $name, $fid);
		}
	}

	private function update_20160710_01($db) {
		// 本次更新： 价格体系表结构调整
		
		// 新增表 t_price_system
		$tableName = "t_price_system";
		
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_price_system` (
						  `id` varchar(255) NOT NULL,
						  `name` varchar(255) NOT NULL,
						  `data_org` varchar(255) DEFAULT NULL,
						  `company_id` varchar(255) DEFAULT NULL,
						  PRIMARY KEY (`id`)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		// 新增表 t_goods_price
		$tableName = "t_goods_price";
		if (! $this->tableExists($db, $tableName)) {
			$sql = "CREATE TABLE IF NOT EXISTS `t_goods_price` (
						  `id` varchar(255) NOT NULL,
						  `goods_id` varchar(255) NOT NULL,
						  `ps_id` varchar(255) NOT NULL,
						  `price` decimal(19,2) NOT NULL,
						  `data_org` varchar(255) DEFAULT NULL,
						  `company_id` varchar(255) DEFAULT NULL,
						  PRIMARY KEY (`id`)
						) ENGINE=InnoDB DEFAULT CHARSET=utf8;
					";
			$db->execute($sql);
		}
		
		// t_customer 表新增字段 ps_id
		$tableName = "t_customer";
		$columnName = "ps_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
		
		// t_customer_category 新增字段 ps_id
		$tableName = "t_customer_category";
		$columnName = "ps_id";
		if (! $this->columnExists($db, $tableName, $columnName)) {
			$sql = "alter table {$tableName} add {$columnName} varchar(255) default null;";
			$db->execute($sql);
		}
	}

	private function update_20160708_01($db) {
		// 本次更新： 新增模块【淘宝店铺管理】
		$fid = FIdConst::TAOBAO_SHOP;
		$name = "淘宝店铺管理";
		$sql = "select count(*) as cnt from t_fid where fid = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_fid(fid, name) value('%s', '%s')";
			$db->execute($sql, $fid, $name);
		}
		
		$sql = "select count(*) as cnt from t_permission where id = '%s' ";
		$data = $db->query($sql, $fid);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$category = "淘宝店宝贝管理";
			$note = "通过菜单进入淘宝店铺管理模块的权限";
			$ps = new PinyinService();
			$py = $ps->toPY($name);
			$sql = "insert into t_permission(id, fid, name, note, category, py)
					value('%s', '%s', '%s', '%s', '%s', '%s')";
			$db->execute($sql, $fid, $fid, $name, $note, $category, $py);
		}
		
		$sql = "select count(*) as cnt from t_menu_item
				where id = '090602' ";
		$data = $db->query($sql);
		$cnt = $data[0]["cnt"];
		if ($cnt == 0) {
			$sql = "insert into t_menu_item(id, caption, fid, parent_id, show_order)
					values ('090602', '%s', '%s', '0906', 2)";
			$db->execute($sql, $name, $fid);
		}
	}
}